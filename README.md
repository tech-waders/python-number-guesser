# Python number guesser
JacenF

## Description
This is a simple number guesser, written 100% in Python. It's contained in one file (main.py).

## Usage
To use this, you first input a min number and then a max number. It chooses a random number. You enter your guess, and it tells you if it's higher or lower, and then you guess again. You then get to replay when you get it right. Literally hours of entertainment.

## Authors and acknowledgment
Code by Jacen Fossey  
Repo organized by @Freddybobjo


