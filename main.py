import random

#This function asks user for a range of their choice by gettin max and min of range
def get_range():
  get_range.range_min = int(input('Enter the minimum number in your range: '))
  get_range.range_max = int(input('Enter the maximum number in your range: '))
  print('Okay, using range:',get_range.range_min,'-',get_range.range_max)

  #This funtion gets the users guess, compares the guess to the selected number, and then gives them a hint based on that
def give_hint(user_num):
  num = random.randint(get_range.range_min, get_range.range_max)
  while get_guess.guess != num:
    if get_guess.guess < num:
      print('Too low!')
      get_guess()
    elif get_guess.guess > num:
      print ('Too high!')
      get_guess()
  if get_guess.guess == num:
    print('Got it!')
    replay()

#This function allows the user to replay. Also includes code to check if they entered anything other than yes or no, and then asks them if they entered it correctly
def replay():
  print('Play again?')
  play_again = input('Enter yes or no: ');
  if play_again == 'yes':
    give_hint(get_guess());
  elif play_again == 'no': 
    print('Okay, goodbye!')
  else:
    print('Did you enter that correctly?')
    replay()
    
#This function gets the guess from the user and then returns it
def get_guess():
  get_guess.guess = int(input('Guess a number: '))
  return get_guess.guess

#Calls get_range()
get_range()

#Calls give_hint with get_guess as argument
give_hint(get_guess())


